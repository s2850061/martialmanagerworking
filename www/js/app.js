// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'firebase'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // setup an abstract state for the tabs directive
    .state('menu', {
      cache: false,
      url: "/menu",
      abstract: true,
      templateUrl: "templates/menu.html"
    })

    .state('menu.studentList', {
      cache: false,
      url: '/studentList',
      views: {
        'menuContent': {
          templateUrl: 'templates/studentList.html',
          controller: 'StudentCtrl'
        }
      }
    })
    
    .state('menu.addStudent', {
      cache: false,
      url: '/addStudent',
      views: {
        'menuContent': {
          templateUrl: 'templates/addStudent.html',
          controller: 'StudentCtrl'
        }
      }
    })
    
    .state('logIn', {
      cache: false,
      url: '/logIn',
        templateUrl: 'templates/login.html',
        controller: 'AccountCtrl'
    })

    .state('menu.userAccount', {
      cache: false,
      url: '/userAccount',
      views: {
        'menuContent': {
          templateUrl: 'templates/userAccount.html',
          controller: 'AccountCtrl'
        }
      }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/logIn');
});
