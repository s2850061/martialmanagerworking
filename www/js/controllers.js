angular.module('starter.controllers', [])

.controller('StudentCtrl', function($scope, fireBaseData, $firebase, $location) {
      $scope.students = $firebase(fireBaseData.refStudents()).$asArray();
        $scope.user = fireBaseData.ref().getAuth();
    
        
      $scope.addStudent = function(e){
        
        if ($scope.studEnd == undefined) {
            $scope.students.$add({
            by: $scope.user.password.email,
            studFirstName: $scope.studFirstName,
            studLastName: $scope.studLastName,
            studEmail: $scope.studEmail,
            studPhone: $scope.studPhone,
            studDOB: $scope.studDOB.toString(),
            studStart: $scope.studStart.toString(),
            studEnd: "undefined",
            payType: $scope.payType,
            contName: $scope.contName,
            contPhone: $scope.contPhone
            })
        }
        
        else {
          $scope.students.$add({
            by: $scope.user.password.email,
            studFirstName: $scope.studFirstName,
            studLastName: $scope.studLastName,
            studEmail: $scope.studEmail,
            studPhone: $scope.studPhone,
            studDOB: $scope.studDOB.toString(),
            studStart: $scope.studStart.toString(),
            studEnd: $scope.studEnd.toString(),
            payType: $scope.payType,
            contName: $scope.contName,
            contPhone: $scope.contPhone
          });
          
        };
          
          $scope.studFirstName = "";
          $scope.studLastName = "";
          $scope.studEmail = "";
          $scope.studPhone = "";
          $scope.studDOB = "";
          $scope.studStart = "";
          $scope.studEnd = "";
          $scope.payType = "";
          $scope.contName = "";
          $scope.contPhone = "";
          $location.path("/menu/studentList");
      };
    
})
.controller('AccountCtrl', function($scope, fireBaseData, $firebase, $location, $window) {
        //$scope.showLoginForm = false;
        //Checking if user is logged in
        $scope.user = fireBaseData.ref().getAuth();
        if (!$scope.user) {
            $scope.showLoginForm = true;
        }
        //Login method
        $scope.login = function (em, pwd) {
            fireBaseData.ref().authWithPassword({
                email    : em,
                password : pwd
            }, function(error, authData) {
                if (error === null) {
                    $location.path("/menu/userAccount");
                    console.log("User ID: " + authData.uid + ", Provider: " + authData.provider);
                    alert("Signed In");
                    $scope.user = fireBaseData.ref().getAuth();
                    $scope.$apply();
                } else {
                    alert(error);
                }
            });
        };
        //Logout method
        $scope.logout = function () {
          fireBaseData.ref().unauth();
          $location.path("/logIn");
          //SPITS OUT Error: permission_denied: Client doesn't have permission to access the desired data.
          //ERROR DOES NOTHING, STILL WORKS
        };
        //Register method
        $scope.register = function(em,pwd) {
            fireBaseData.ref().createUser({
                  email: em,
                  password: pwd
                  },function(error) {
                        if (error) {
                        alert(error);
                        } else {
                        alert("Registration Successful: Please Sign In");
                        }
                  });
        };
});