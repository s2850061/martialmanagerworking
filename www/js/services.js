angular.module('starter.services', [])
/**
 * A simple example service that returns some data.
 */
.factory('fireBaseData', function($firebase) {
  var ref = new Firebase("https://test-fire1010.firebaseio.com/"),
      refStudents = new Firebase("https://test-fire1010.firebaseio.com/")
  return {
    ref: function () {
      return ref;
    },
    refStudents: function () {
      return refStudents;
    }
  }
});